/**
 * [[Archivo global del proyecto]]
 * @private
 * @author Luis Gerardo Ramirez Carmona
 */
( function( $ ) {
	var init = () => {
		if($('img.lazy')) lazyLoadAssets();
		if($('img.imgResp')) changeImageResp();
		if($('.community .slider,.news .slider')) slidersHome();
		if($('section.intro')) menuIntroProgramas();
		if($('#menuAgemda')) menuParaAgenda();
		if($('.c-acordeon__lista-titulo')) proyectos();
		if($('#c-tabs__tabs')) menejoDeTabs();
		if($('.c-speakers')) speakers();
		menu();
		$('select').formSelect();
		if($(".foro-idea-anteriores")) colores();
	};
	
	
	/*!
	 * jQuery Scrollspy Plugin
	 * Author: @sxalexander
	 * Licensed under the MIT license
	 */
	(function ( $, window, document, undefined ) {
		
		$.fn.extend({
			scrollspy: function ( options ) {
				
				var defaults = {
					min: 0,
					max: 0,
					mode: 'vertical',
					namespace: 'scrollspy',
					buffer: 0,
					container: window,
					onEnter: options.onEnter ? options.onEnter : [],
					onLeave: options.onLeave ? options.onLeave : [],
					onTick: options.onTick ? options.onTick : []
				}
				
				var options = $.extend( {}, defaults, options );
				
				return this.each(function (i) {
					
					var element = this;
					var o = options;
					var $container = $(o.container);
					var mode = o.mode;
					var buffer = o.buffer;
					var enters = leaves = 0;
					var inside = false;
					
					/* add listener to container */
					$container.bind('scroll.' + o.namespace, function(e){
						var position = {top: $(this).scrollTop(), left: $(this).scrollLeft()};
						var xy = (mode == 'vertical') ? position.top + buffer : position.left + buffer;
						var max = o.max;
						var min = o.min;
						
						/* fix max */
						if($.isFunction(o.max)){
							max = o.max();
						}
						
						/* fix max */
						if($.isFunction(o.min)){
							min = o.min();
						}
						
						if(max == 0){
							max = (mode == 'vertical') ? $container.height() : $container.outerWidth() + $(element).outerWidth();
						}
						
						/* if we have reached the minimum bound but are below the max ... */
						if(xy >= min && xy <= max){
							/* trigger enter event */
							if(!inside){
								inside = true;
								enters++;
								
								/* fire enter event */
								$(element).trigger('scrollEnter', {position: position})
								if($.isFunction(o.onEnter)){
									o.onEnter(element, position);
								}
								
							}
							
							/* trigger tick event */
							$(element).trigger('scrollTick', {position: position, inside: inside, enters: enters, leaves: leaves})
							if($.isFunction(o.onTick)){
								o.onTick(element, position, inside, enters, leaves);
							}
						}else{
							
							if(inside){
								inside = false;
								leaves++;
								/* trigger leave event */
								$(element).trigger('scrollLeave', {position: position, leaves:leaves})
								
								if($.isFunction(o.onLeave)){
									o.onLeave(element, position);
								}
							}
						}
					});
					
				});
			}
			
		})
		
		
	})( jQuery, window, document, undefined );
	
	
	
	/**
	 * @description Carga diferida de imagenes
	 * */
	var lazyLoadAssets = () => {
		document.addEventListener("DOMContentLoaded", function() {
			var lazyloadImages;
			if ("IntersectionObserver" in window) {
				lazyloadImages = document.querySelectorAll(".lazy");
				var imageObserver = new IntersectionObserver(function(entries, observer) {
					entries.forEach(function(entry) {
						if (entry.isIntersecting) {
							var image = entry.target;
							image.src = image.dataset.src;
							image.classList.remove("lazy");
							imageObserver.unobserve(image);
						}
					});
				});
				lazyloadImages.forEach(function(image) {
					imageObserver.observe(image);
				});
			} else {
				console.info('no soportado');
				notSupportedLazy();
				$(document).on('scroll', function() {
					notSupportedLazy();
				})
			}
		})
	};
	
	/**
	 * @description Si no es soportada la carga diferida de imagenes
	 * */
	var notSupportedLazy = () => {
		var imgLazy = $('.lazy'),
			timeLazy,
			scrollTop;
		scrollTop = $(window).scrollTop();
		for (var i = 0; i < imgLazy.length; i++) {
			if ($(imgLazy[i]).offset().top < ($(window).height() + scrollTop)) {
				$(imgLazy[i]).attr('src', $(imgLazy[i]).attr('data-src'));
				$(imgLazy[i]).removeClass('lazy');
			}
		}
	}
	
	/**
	 * @description Manejo de images responsive
	 * */
	var changeImageResp = () => {
		var image = $('.imgResp'),
			imgDesk,
			imgMob;
		
		for (var i = 0; i < image.length; i++) {
			imgDesk = $(image[i]).attr('data-desktop');
			imgMob = $(image[i]).attr('data-mobile');
			
			if ($(window).width() <= 768) {
				$(image[i]).attr('src', imgMob);
			} else {
				$(image[i]).attr('src', imgDesk);
			}
		}
	};
	
	/**
	 * @description Sliders del home
	 */
	function slidersHome(){
		$('.community .slider,.news .slider').slick({
			autoplay: true,
			autoplaySpeed:2000,
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 3,
			prevArrow:'<div class="prevArrow"></div>',
			nextArrow:'<div class="nextArrow"></div>',
			responsive: [
				{
					breakpoint: 980,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					}
				}
			]
		});
	}
	
	function menuIntroProgramas(){
		let menu = $('#menuIntro li a');
		
		menu.on('click', function(evento){
			evento.preventDefault();
			
			if(!$(this).hasClass('active')){
				$(this).parents('li').siblings('li').find('a').removeClass('active');
				$(this).addClass('active');
				
				let contenido = $(this).attr('href')
				
				$('section .texto').each(function(){
					$(this).removeClass('active');
					
					if(contenido === `#${$(this).attr('id')}`){
						$(this).addClass('active');
					}
				});
			}
		});
	}
	
	function menuParaAgenda(){
		let botones = $('#menuAgemda a');
		
		botones.on('click', function(evento){
			evento.preventDefault();
			
			let elemento = $(this).attr('href');
			
			$('.contenido-agenda').removeClass('active');
			$('.contenido-agenda').hide();
			
			$(elemento).slideDown('slow');
		});
	}
	
	function proyectos(){
		let botones = $('.c-acordeon__lista-titulo');
		let hash = window.location.hash;
		
		console.log(hash);
		
		if(hash !== ''){
			botones.each(function(){
				if($(this).attr("data-href") === hash){
					let that = $(this);
					that.addClass('is-active');
					that.next('dd').slideToggle("slow", function(){
						$('html, body').animate({scrollTop: ($(hash).offset().top - (that.innerHeight()) - ($('header').innerHeight()))}, 800);
					});
				}
			});
		}
		
		botones.on('click', function(evento){
			evento.preventDefault();
			
			$(this).toggleClass('is-active');
			
			$(this).next('dd').slideToggle('slow');
		});
	}
	
	$(document).on('change', '.file-field input[type="file"]', function () {
		console.log('entre');
		let file_field = $(this).closest('.file-field');
		let path_input = file_field.find('input.file-path');
		let files = $(this)[0].files;
		let file_names = [];
		for (let i = 0; i < files.length; i++) {
			file_names.push(files[i].name);
		}
		path_input[0].value = file_names.join(', ');
		path_input.trigger('change');
	});
	
	/**
	* @description Funcionalidad menu
	* */
	function menu(){
		let boton = $('#menu-mobil, #cerrar-menu');
		
		boton.on('click', function(evento){
			evento.preventDefault();
			
			$('body').toggleClass('active');
		});
	}
	
	function menejoDeTabs(){
		let contenidotabs = $('#c-tabs__tabs');
		
		contenidotabs.find('dt').on('click', function(evento){
			evento.preventDefault();
			
			$(this).siblings('dd').not($(this).next('dd')).hide();
			$(this).next('dd').slideToggle('slow');
			
			$(this).siblings('dt').removeClass('active');
			$(this).toggleClass('active');
		});
	}
	
	function speakers(){
		let item = $('.c-speakers__lista--item');
		let label = $('.c-speakers__nombre-ponente');
		
		item.on('click', function(evento){
			let enlace = $(this).attr("data-href");
			let items = $('.c-speakers__informacion li');
			
			$(this).siblings('li').removeClass("active");
			$(this).addClass("active");
			
			items.removeClass('active');
			console.log(enlace);
			$(enlace).addClass("active");
			
			$(".c-speakers__lista").toggleClass('is-active');
			
			label.html($(this).html());
		});
		
		label.on('click', function(){
			$(this).toggleClass('is-active');
			
			$(".c-speakers__lista").toggleClass('is-active');
		});
	}
	
	function colores(){
		let color = $("main").attr("data-color");
		let backgroundColor = $(".js-background-color");
		let textoColor = $(".js-texto-color");
		
		backgroundColor.css({background: color});
		textoColor.css({color: color});
		
		$('head').append(`
			<style>
				.c-imagen-texto__texto--lista li:before{
					border:3px solid ${color}
				}
				.c-tabs--verde .c-tabs__cifras li:before{
					background-color: ${color};
				}
				.c-speakers__nombre-ponente:after{
					border-bottom: 2px solid ${color};
					border-left: 2px solid ${color};
				}
				.c-speakers__lista--item:before, .c-speakers__lista--item:after{
					background-color: ${color};
				}
				.c-speakers__lista--item.active{
					color: ${color};
				}
			</style>`
		);
	}
	
	$(document).ready(function() {
		if($('.c-menu-agenda a').length){
			$('.c-menu-agenda a').bind('click', function(e) {
				e.preventDefault(); // prevent hard jump, the default behavior
				
				var target = $(this).attr("href"); // Set the target as variable
				
				// perform animated scrolling by getting top-position of target-element and set it as scroll target
				$('html, body').stop().animate({
					scrollTop: $(target).offset().top - 150
				}, 600, function() {
					location.hash = target; //attach the hash (#jumptarget) to the pageurl
				});
				
				return false;
			});
		}
	});
	
	$(window).scroll(function() {
		var scrollDistance = $(window).scrollTop() + 150;
		
		if($('.scrolling').length){
			$('.scrolling').each(function(i) {
				if ($(this).position().top <= scrollDistance) {
					$('.c-menu-agenda a.active').removeClass('active');
					$('.c-menu-agenda a').eq(i).addClass('active');
				}
			});
		}
	}).scroll();
	
	init();
}(jQuery));

