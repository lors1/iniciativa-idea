"use strict";

const {src, dest, series, watch, parallel} = require('gulp'),
	sourcemaps = require('gulp-sourcemaps'),
	sass = require('gulp-sass'),
	gcmq = require('gulp-group-css-media-queries'),
	cssmin = require('gulp-cssmin'),
	autoprefixer = require('gulp-autoprefixer'),
	rename = require("gulp-rename"),
	pug = require('gulp-pug'),
	fs = require('fs'),
	decomment = require('gulp-decomment'),
	babel = require('gulp-babel'),
	concat = require('gulp-concat'),
	minify = require("gulp-babel-minify"),
	browsersync = require("browser-sync").create();

const configuracion = JSON.parse(fs.readFileSync("config.json", "utf8"));
sass.compiler = require('node-sass');

function htmlDesarrollo(done){
	if(typeof done !== "function"){
		let variable = done,
			variableRota = done.split("/"),
			variableFinal = [],
			folder;
		
		for(let i = 2; i < variableRota.length -1; i++) {
			variableFinal = variableRota[i];
		}

		folder = `dist/dev/${variableFinal}`

		if(variable.indexOf("_") === -1){
			return src(variable)
				.pipe(pug({doctype: 'html', pretty: true}).on('error', error => {console.log('Error en pug: ' + error);}))
				.pipe(dest(folder))
				.pipe(browsersync.stream());
		}else{
			return src(configuracion.directorios.pug.fuente)
				.pipe(pug({doctype: 'html', pretty: true}).on('error', error => {console.log('Error en pug: ' + error);}))
				.pipe(dest(configuracion.directorios.pug.destino.dev))
				.pipe(browsersync.stream());
		}
	}else{
		return src(configuracion.directorios.pug.fuente)
			.pipe(pug({doctype: 'html', pretty: true}).on('error', error => {console.log('Error en pug: ' + error);}))
			.pipe(dest(configuracion.directorios.pug.destino.dev))
			.pipe(browsersync.stream());
	}
}

function htmlProduccion(done){
	if(typeof done !== "function"){
		let variable = done,
			variableRota = done.split("/"),
			variableFinal = [],
			folder;
		
		for(let i = 2; i < variableRota.length -1; i++) {
			variableFinal = variableRota[i];
		}
		
		folder = `dist/prod/${variableFinal}`
		
		if(variable.indexOf("_") === -1){
			return src(variable)
				.pipe(pug({doctype: 'html', pretty: false}).on('error', error => {console.log('Error en pug: ' + error);}))
				.pipe(dest(folder))
				.pipe(browsersync.stream());
		}else{
			return src(configuracion.directorios.pug.fuente)
				.pipe(pug({doctype: 'html', pretty: false}).on('error', error => {console.log('Error en pug: ' + error);}))
				.pipe(dest(configuracion.directorios.pug.destino.prod))
				.pipe(browsersync.stream());
		}
	}else{
		return src(configuracion.directorios.pug.fuente)
			.pipe(pug({doctype: 'html', pretty: false}).on('error', error => {console.log('Error en pug: ' + error);}))
			.pipe(dest(configuracion.directorios.pug.destino.prod))
			.pipe(browsersync.stream());
	}
}

function cssDesarrollo(){
	return src(configuracion.directorios.sass.fuente)
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(sourcemaps.write())
		.pipe(dest(configuracion.directorios.sass.destino.dev))
		.pipe(browsersync.stream());
}

function cssProduccion(){
	return src(configuracion.directorios.sass.fuente)
		.pipe(sass().on('error', sass.logError))
		.pipe(gcmq())
		.pipe(autoprefixer())
		.pipe(cssmin())
		.pipe(rename({suffix: ".min"}))
		.pipe(dest(configuracion.directorios.sass.destino.prod))
		.pipe(browsersync.stream());
}

function jsDesarrollo(){
	return src(configuracion.directorios.js.fuente)
		//.pipe(sourcemaps.init())
		.pipe(babel({plugins:configuracion.directorios.js.babelPlugins}))
		.pipe(concat('main.js'))
		//.pipe(sourcemaps.write())
		.pipe(dest(configuracion.directorios.js.destino.dev))
		.pipe(browsersync.stream());
}

function jsProduccion(){
	return src(configuracion.directorios.js.fuente)
		.pipe(decomment({trim: true}))
		.pipe(babel({plugins:configuracion.directorios.js.babelPlugins}))
		.pipe(concat('main.js'))
		.pipe(minify({mangle: {keepClassName: true}}))
		.pipe(rename({suffix: ".min"}))
		.pipe(dest(configuracion.directorios.js.destino.prod))
		.pipe(browsersync.stream());
}

function imagenesDesarrollo(){
	return src(configuracion.directorios.imagenes.fuente)
		.pipe(dest(configuracion.directorios.imagenes.destino.dev))
}

function imagenesProduccion(){
	return src(configuracion.directorios.imagenes.fuente)
		.pipe(dest(configuracion.directorios.imagenes.destino.prod))
}

function fontsDesarrollo(){
	return src(configuracion.directorios.fonts.fuente)
		.pipe(dest(configuracion.directorios.fonts.destino.dev))
}

function fontsProduccion(){
	return src(configuracion.directorios.fonts.fuente)
		.pipe(dest(configuracion.directorios.fonts.destino.prod))
}

function videoDesarrollo(){
	return src(configuracion.directorios.video.fuente)
		.pipe(dest(configuracion.directorios.video.destino.dev))
}

function videoProduccion(){
	return src(configuracion.directorios.video.fuente)
		.pipe(dest(configuracion.directorios.video.destino.prod))
}

function browserSyncDesarrollo(done){
	browsersync.init({
		server: {
			baseDir: "dist/dev",
			serveStaticOptions: {
				extensions: ['html']
			},
			directory: true
		},
		port: 3000
	});
	
	let watcherHtml = watch(configuracion.directorios.pug.watcher);
	
	watch(configuracion.directorios.sass.watcher, cssDesarrollo);
	watch(configuracion.directorios.js.watcher,     jsDesarrollo);
	watch(configuracion.directorios.imagenes.fuente,imagenesDesarrollo);
	watch(configuracion.directorios.fonts.fuente,   fontsDesarrollo);
	watch(configuracion.directorios.video.fuente,   videoDesarrollo);
	watcherHtml.on('change', function(path){htmlDesarrollo(path)});
	
	watch(configuracion.directorios.js.recargar.dev).on('change', browsersync.reload);
	watch(configuracion.directorios.pug.recargar.dev).on('change', browsersync.reload);
	
	done();
}

function browserSyncProduccion(done){
	browsersync.init({
		server: {
			baseDir: "dist/prod",
			serveStaticOptions: {
				extensions: ['html']
			},
			directory: true
		},
		port: 3000
	});
	
	let watcherHtml = watch(configuracion.directorios.pug.watcher);
	
	watch(configuracion.directorios.sass.watcher, cssProduccion);
	watch(configuracion.directorios.js.watcher,     jsProduccion);
	watch(configuracion.directorios.imagenes.fuente,imagenesProduccion);
	watch(configuracion.directorios.fonts.fuente,   fontsProduccion);
	watch(configuracion.directorios.video.fuente,   videoProduccion);
	watcherHtml.on('change', function(path){htmlProduccion(path)});
	
	watch(configuracion.directorios.js.recargar.dev).on('change', browsersync.reload);
	watch(configuracion.directorios.pug.recargar.dev).on('change', browsersync.reload);
	
	done();
}

function browserSyncGeneral(done){
	browsersync.init({
		server: {
			baseDir: "dist/dev",
			serveStaticOptions: {
				extensions: ['html']
			},
			directory: true
		},
		port: 3000
	});
	
	let watcherHtml = watch(configuracion.directorios.pug.watcher);
	
	watch(configuracion.directorios.sass.watcher, function (){cssProduccion; cssDesarrollo});
	watch(configuracion.directorios.js.watcher,     function (){jsProduccion; jsDesarrollo});
	watch(configuracion.directorios.imagenes.fuente,function (){imagenesProduccion; imagenesDesarrollo});
	watch(configuracion.directorios.fonts.fuente,   function (){fontsProduccion; fontsDesarrollo});
	watch(configuracion.directorios.video.fuente,   function (){videoProduccion; videoDesarrollo});
	watcherHtml.on('change', function(path){htmlProduccion(path); htmlDesarrollo(path)});
	
	watch(configuracion.directorios.js.recargar.dev).on('change', browsersync.reload);
	watch(configuracion.directorios.pug.recargar.dev).on('change', browsersync.reload);
	
	done();
}

exports.htmlDesarrollo = htmlDesarrollo;
exports.htmlProduccion = htmlProduccion;
exports.cssDesarrollo = cssDesarrollo;
exports.cssProduccion = cssProduccion;
exports.jsDesarrollo = jsDesarrollo;
exports.jsProduccion = jsProduccion;
exports.imagenesDesarrollo = imagenesDesarrollo;
exports.imagenesProduccion = imagenesProduccion;
exports.fontsDesarrollo = fontsDesarrollo;
exports.fontsProduccion = fontsProduccion;
exports.videoDesarrollo = videoDesarrollo;
exports.videoProduccion = videoProduccion;

const compilarDesarrollo = series(parallel(cssDesarrollo, jsDesarrollo, htmlDesarrollo, imagenesDesarrollo, fontsDesarrollo, videoDesarrollo), browserSyncDesarrollo);
const compilarProduccion = series(parallel(cssProduccion, jsProduccion, htmlProduccion, imagenesProduccion, fontsProduccion, videoProduccion), browserSyncProduccion);
const compilarGeneral = series(parallel(cssDesarrollo, jsDesarrollo, htmlDesarrollo, imagenesDesarrollo, fontsDesarrollo, cssProduccion, jsProduccion, htmlProduccion, imagenesProduccion, fontsProduccion), browserSyncGeneral);

exports.default = compilarDesarrollo;
exports.produccion = compilarProduccion;
exports.general = compilarGeneral;
